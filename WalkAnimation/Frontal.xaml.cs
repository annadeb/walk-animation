﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Markup;
using System.Windows.Shapes;

namespace WalkAnimation
{
    /// <summary>
    /// Logika interakcji dla klasy Czolowa.xaml
    /// </summary>
    public partial class Frontal : Page
    {
        public Frontal()
        {
            InitializeComponent();
            DataContext = BodyParts.GetBodyParts();
        }
      
        private void BRightArm_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BLeftArm_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BRightLeg_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BLeftLeg_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BOne_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BTwo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BThree_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BFour_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
