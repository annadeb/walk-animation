﻿using System;
using System.IO;
using System.Windows;

namespace WalkAnimation
{
    internal class BodyParts
    {
        public double ArmWidth { get; set; }
        public double ArmHeight { get; set; }
        public double LegWidth { get; set; }
        public double LegHeight { get; set; }
        public double HeadHeight { get; set; }
        public double BodyHeight { get; set; }
        public double HeadWidth { get; set; }
        public double BodyWidth { get; set; }


        public static BodyParts GetBodyParts()
        {
            double[] result = ReadPoints();
            var bp = new BodyParts()
            {
                HeadHeight = result[0] / 1.5,
                HeadWidth = 50,
                ArmHeight = 19,
                ArmWidth = result[1]/1.33,
                LegHeight = 19,
                LegWidth = result[2] / 1.71,
                BodyHeight = 50,
                BodyWidth = result[3]
            };
            return bp;
        }

        private static double[] ReadPoints()
        {
            double headHeight = 0;
            double armWidth = 0;
            double legWidth = 0;
            double bodyHeight = 0;

            string fileName = "Points3.txt";
            string path = Path.Combine(Environment.CurrentDirectory, @"Data\", fileName);
            StreamReader strmRd = new StreamReader(path);
            string data = strmRd.ReadToEnd();
            string[] lines = data.Split('|');
            Point head = new Point(0, 0);
            Point rArm = new Point(0, 0);
            Point lArm = new Point(0, 0);
            Point rLeg = new Point(0, 0);
            Point lLeg = new Point(0, 0);
            Point w1 = new Point(0, 0);
            Point w2 = new Point(0, 0);
            for (int i = 0; i < lines.Length; i++)
            {
                string[] row = lines[i].Split(';');
                switch (row[0])
                {
                    case "glowa":
                        head = new Point(int.Parse(row[1]), int.Parse(row[2]));
                        break;
                    case "pReka":
                        rArm = new Point(int.Parse(row[1]), int.Parse(row[2]));
                        break;
                    case "lReka":
                        lArm = new Point(int.Parse(row[1]), int.Parse(row[2]));
                        break;
                    case "pNoga":
                        rLeg = new Point(int.Parse(row[1]), int.Parse(row[2]));
                        break;
                    case "lNoga":
                        lLeg = new Point(int.Parse(row[1]), int.Parse(row[2]));
                        break;
                    case "wezel1":
                        w1 = new Point(int.Parse(row[1]), int.Parse(row[2]));
                        break;
                    case "wezel2":
                        w2 = new Point(int.Parse(row[1]), int.Parse(row[2]));
                        break;
                    default:
                        break;
                }
            }
            headHeight = w1.Y - head.Y;

            double armLeng1 = w1.X - lArm.X;
            double armLeng2 = w1.Y - lArm.Y;
            armWidth = Math.Sqrt((armLeng1 * armLeng1) + (armLeng2 * armLeng2));

            double legLeng1 = w1.X - lLeg.X;
            double legLeng2 = w1.Y - lLeg.Y;
            legWidth = Math.Sqrt((legLeng1 * legLeng1) + (legLeng2 * legLeng2));

            bodyHeight = w2.Y - w1.Y;

            double[] result = new double[] { headHeight, armWidth, legWidth, bodyHeight };
            return result;
        }

    }
}